﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceBehaviour : MonoBehaviour
{
    [Tooltip("Flag for if the user seed below is used to generate dice rolls or not.")]
    public bool userSetSeed;
    [Tooltip("Seed to base dice rolls off of. If not used, seed is based on internal time.")]
    public int userSeed;
    [Tooltip("How long the die rolls when the button to roll is clicked.")]
    public float rollTime;

    
    GameObject displayHeader;    
    GameObject numberDisplay;  // text area that displays the die result    
    GameObject dieDisplay;  // GameObject that displays the die face
    GameObject startRollButton;    
    Sprite[] diceFaces;  // List of die face sprites, loaded in on runtime.

    int generatedRandomNumber;
    int seed;  // Seed that is either user-input or time generated
    Vector3 diePos = new Vector3(0, 0.5f, 0);

	/// <summary>
    /// Start gets the display area, loads in the dice sprites, initializes the
    /// random generation seed, and hides text displays
    /// </summary>
	void Start ()
    {
        displayHeader = GameObject.Find("Roll Header");
        numberDisplay = GameObject.Find("Roll Result");
        startRollButton = GameObject.Find("Roll Button");
        displayHeader.SetActive(false);
        numberDisplay.GetComponent<Text>().text = "";
        InitializeSeed();
        LoadDiceSprites();                
        CreateDieObject();
    }

    /// <summary>
    /// CreateDieObject creates the game object that displays the die faces
    /// </summary>
    void CreateDieObject()
    {
        dieDisplay = new GameObject();
        dieDisplay.AddComponent<SpriteRenderer>();
        dieDisplay.GetComponent<SpriteRenderer>().sprite = diceFaces[0];
        dieDisplay.transform.position = diePos;
        dieDisplay.gameObject.name = "Die";
        dieDisplay.gameObject.tag = "Die";
    }
	
	/// <summary>
    /// InitializeSeed initialized the random generation seed.
    /// InitializeSeed either creates a new seed based on the time,
    /// or uses a user-set seed if the option is selected in the editor.
    /// </summary>
    void InitializeSeed()
    {
        if (userSetSeed)
            seed = userSeed;
        else
            seed = (int)System.DateTime.Now.Ticks;
        Random.InitState(seed);
    }

    /// <summary>
    /// LoadDiceSprites loads in the dice sprite sheet from the Resources folder
    /// </summary>
    void LoadDiceSprites()
    {
        diceFaces = Resources.LoadAll<Sprite>("Dice");
    }

    /// <summary>
    /// RollDie calculates the result of the die roll and displays the
    /// result in the text area and through the dice sprite
    /// </summary>
    public void RollDie()
    {
        displayHeader.SetActive(false);
        numberDisplay.GetComponent<Text>().text = "";
        startRollButton.SetActive(false);
        generatedRandomNumber = Random.Range((int)1, (int)7);
        StartCoroutine("AnimateDieRoll");
    }

    /// <summary>
    /// AnimateDieRoll simulates a die roll by rapidly changing the 
    /// die face sprites, then landing on the result when it is done.
    /// </summary>
    IEnumerator AnimateDieRoll()
    {
        float timeStep = 0;
        while(timeStep < rollTime)
        {
            int randomFace = Random.Range((int)0, (int)6);
            dieDisplay.GetComponent<SpriteRenderer>().sprite = diceFaces[randomFace];
            yield return new WaitForEndOfFrame();
            timeStep += Time.deltaTime;
        }
        displayHeader.SetActive(true);
        numberDisplay.GetComponent<Text>().text = generatedRandomNumber.ToString();
        dieDisplay.GetComponent<SpriteRenderer>().sprite = diceFaces[generatedRandomNumber - 1];
        startRollButton.SetActive(true);
        
    }    

    #region Debug
    /// <summary>
    /// DisplayDiceSprites is a debug function.
    /// It is called to make sure the die faces load into the sprite list correctly.
    /// </summary>
    void DisplayDiceSprites()
    {
        for(int i = 0; i < diceFaces.Length; i++)
        {
            GameObject tempFace = new GameObject();
            tempFace.AddComponent<SpriteRenderer>();
            tempFace.GetComponent<SpriteRenderer>().sprite = diceFaces[i];
            tempFace.transform.position = new Vector3(i, 0);
        }
    }
    #endregion
}
